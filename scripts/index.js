const http = require('http');

require('dotenv').config();
const PORT = process.env.PORT;

const fs = require('fs');
const path = require('path');
const sort1 = require("./sort");
const sort2 = require('./sort2.js');
const sort3 = require('./sort3.js');
const sort4 = require('./sort4.js');
const sort5 = require('./sort5.js');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public'); 


function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
    switch(req.url) {
      case "/":
        res.writeHead(200)
        res.end(getHTML("../public/index.html"))
        return;
      case "/data1":
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(toJSON(sort1))
        return;
      case "/data2":
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(toJSON(sort2))
        return;
      case "/data3":
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(toJSON(sort3))
        return;
      case "/data4":
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(toJSON(sort4))
        return;
      case "/data5":
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(toJSON(sort5))
        return;
      case "/about":
        res.writeHead(200)
        res.end(getHTML("../public/about.html"))
        return;
      default:
        res.writeHead(404)
        res.end(getHTML("../public/404.html"))
        return;
    }
  }

  function toJSON(value) {
    return JSON.stringify(value);
  }

  const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, '0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})

// console.log(data);
