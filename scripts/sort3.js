const data = require("./data.js");

function sortData3 (data) {
// Tempat penampungan hasil
const result = [];

for ( let i = 0; i < data.length; i++) {
    // warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel
    if ( data[i].eyeColor === "blue" && data[i].age >= 35 && data[i].age <= 40 && data[i].favoriteFruit === "apple" ) { 
        result.push(data[i]);
        }
    }

return result;
}

module.exports = sortData3(data)
