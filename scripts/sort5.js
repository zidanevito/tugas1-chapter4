const data = require("./data.js");

function sortData5 (data) {
// Tempat penampungan hasil
const result = [];

for ( let i = 0; i < data.length; i++) {
    // registered di bawah tahun 2016 dan masih active(true)
    if ( data[i].registered < "2016-10-11T12:25:56 -07:00" && data[i].isActive ) { 
        result.push(data[i]);
        }
    }

return result;
}

module.exports = sortData5(data);
